/* run ruby code with an exposed Global Policy object.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <exception>

#include "mruby.h"
#include "mruby/compile.h"
#include "mruby/proc.h"

#include "../Policy/include/Policy.h"

using namespace std;

extern "C" {
  void MPolicy_Init(mrb_state *mrb, Policy *mp);
}

void _error(const char* s){
  printf("ERROR: %s\n", s);
  exit(1);
}

int main(int argc, char *argv[])
{
  mrb_state *mrb;
  int n;
  struct mrb_parser_state *p;
  mrbc_context *mrbc_ctx;
  FILE* f;

  if (argc < 2) {
      cerr << "Need a script!" << endl;
      exit(2);
  }
  // Setup the master policy to pass into Ruby
  //
  Policy *masterpol = new Policy(111);

  mrb = mrb_open();
  MPolicy_Init(mrb, masterpol);

  cout << "Master Policy created as M::Policy::MASTER";
  cout << " (Value " << masterpol->calculateValue() << ")" << endl;

  mrbc_ctx = mrbc_context_new(mrb);
  for (int i = 1; i < argc; i++) {
    cout << endl << "Executing script: " << argv[i] << endl;

    f = fopen(argv[i], "r");
    if(f==NULL){ _error("file not found."); }
    p = mrb_parse_file(mrb,f,mrbc_ctx);
    fclose(f);
    n = mrb_generate_code(mrb, p);
    mrb_parser_free(p);
    mrbc_context_free(mrb,mrbc_ctx);
    mrb_run(mrb, mrb_proc_new(mrb, mrb->irep[n]), mrb_nil_value());
    if (mrb->exc) {
     mrb_p(mrb, mrb_obj_value(mrb->exc));
    }
  }
  cout << endl << "Master Policy value " << masterpol->calculateValue() << endl;
  mrb_close(mrb);
  return 0; /* returns 255 otherwise */
}
/* vim: set ts=8 sw=2 sts=2: */
