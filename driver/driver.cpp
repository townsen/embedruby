/*
 * C++ program to show how to embed MRuby interpreter
 */

#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include <ctime>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <mruby.h>
#include <mruby/compile.h>
#include <mruby/proc.h>
#include <mruby/string.h>

#include <Policy.h>

using namespace std;
using namespace boost::posix_time;

static mrb_state *_mrb = NULL;

void initMRuby() {
    if (_mrb == NULL) {
	cout << "Initializing MRuby...";
	ptime start = microsec_clock::local_time();
	_mrb = mrb_open();
	ptime ready = microsec_clock::local_time();
	time_duration tdLoad = ready - start;
	cout << "done in: " << tdLoad << endl;
    } else 
	cout << "MRuby already initialized" << endl;
}
string getScript(char *arg) {
    string script;
	if (arg[0] != '@') {
	    script = arg;
	}
	else {
	    string filename(arg+1);
	    cout << "Reading script from file '" << filename << "'...";
	    ifstream inputfile(filename.c_str(), ifstream::in);
	    if (!inputfile) {
		cout << "NOT FOUND" << endl;
		exit(2);
	    }
	    else {
		cout << "done" << endl;
	    }
	    string inputline;
	    while (!inputfile.eof()) {
		getline(inputfile, inputline);
		if (inputline.length() > 0) {
		    script += "\n";
		    script += inputline;
		}
		if (inputline[0] == '#') break;
	    }
	    inputfile.close();
	}
	if (script.length() == 0) {
	    cout << "Script file '" << arg << "' is empty!" << endl;
	    exit(1);
	}
    return script;
}

int rubyScript(string script) {

    cout << "Parsing script...";
    ptime ready = microsec_clock::local_time();
    mrbc_context *mrbc = mrbc_context_new(_mrb);
    struct mrb_parser_state *p = mrb_parse_string(_mrb, script.c_str(), mrbc);
    int n = mrb_generate_code(_mrb, p);
    ptime parsed = microsec_clock::local_time();
    time_duration tdParse = parsed - ready;
    cout << "done in: " << tdParse << endl;
    cout << "Executing the script: " << script << endl;
    ptime go = microsec_clock::local_time();
    mrb_value result = mrb_run(_mrb, mrb_proc_new(_mrb, _mrb->irep[n]), mrb_top_self(_mrb)); /* note that last param can be mrb_nil_value() */
    ptime done = microsec_clock::local_time();
    time_duration td = done - go;
    if (_mrb->exc) {
	mrb_value msg = mrb_funcall(_mrb, mrb_obj_value(_mrb->exc), "message", 0);
	cout << "Exception:" << mrb_string_value_cstr(_mrb, &msg) << endl;
	//mrb_p(_mrb, mrb_obj_value(_mrb->exc));
    }
    else {
	cout << "Completed" << endl;
	switch (mrb_type(result)) {
	    case MRB_TT_OBJECT:
		{
		mrb_value inspect = mrb_funcall(_mrb, result, "inspect", 0);
		cout << mrb_string_value_cstr(_mrb, &inspect) << " (Object";
		break;
		}
	    case MRB_TT_FIXNUM:
		cout << mrb_fixnum(result) << " (Fixnum";
		break;
	    case MRB_TT_STRING:
//		cout << RSTRING_PTR(result) << " (String";
		cout << mrb_string_value_cstr(_mrb, &result) << " (String";
		break;
	    case MRB_TT_FLOAT:
		cout << mrb_float(result) << " (Float";
		break;
	    case MRB_TT_ARRAY:
		cout << " (Array";
		break;
	    default:
		mrb_p(_mrb, result);
		cout << " (Type " << mrb_type(result);
		break;
	}
	cout << " interpreted in: " << td << ")" << endl;
	// cout << " (" << td.total_microseconds() << "�S)" << endl; // utf8 on windows FAIL
    }
    return 0;
}

int main(int argc, char* argv[]) {

    if (argc < 2) {
	cerr << "Need a script!" << endl;
	exit(2);
    }
    initMRuby();

    for (int i = 1; i < argc; i++) {
	string script = getScript(argv[i]);
	cout << "Executing script: " << script << endl;
	rubyScript(script);
    }
    cout << "Done " << endl;
    exit(0);
}

/* vim: set ts=8 sw=4 sts=4: */
