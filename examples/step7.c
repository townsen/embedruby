/*
 [step7] test equality API calls
*/

#include "mruby.h"
#include "mruby/compile.h"
#include "mruby/proc.h"
#include "mruby/string.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

void _error(const char* s){
  printf("ERROR: %s\n", s);
  exit(1);
}

mrb_value checkme(mrb_state *mrb, mrb_value self)
{
  mrb_value arg1;
  mrb_value arg2;
  int res;

  mrb_get_args(mrb, "oo", &arg1, &arg2);

  /* test in order of severity */
  res = mrb_obj_equal(mrb, arg1, arg2);
  printf("mrb_obj_equal: %d\n",res);
  res = mrb_eql(mrb, arg1, arg2);
  printf("mrb_eql: %d\n",res);
  res = mrb_equal(mrb, arg1, arg2);
  printf("mrb_equal: %d\n",res);

  xfree(arg1);
  return mrb_nil_value();
}

int main()
{
  mrb_state *mrb;
  int n;
  FILE* f;
  struct RClass* cObject;
  mrbc_context *mrbc_ctx;
  struct mrb_parser_state *p;

  mrb = mrb_open();
  mrbc_ctx = mrbc_context_new(mrb);
  cObject = mrb_class_obj_get(mrb, "Object");
  mrb_define_method(mrb, cObject, "checkme", checkme, ARGS_ANY());
  f = fopen("step7.rb", "r");
  if(f==NULL){ _error("file not found."); }
  p = mrb_parse_file(mrb,f,mrbc_ctx);
  fclose(f);
  n = mrb_generate_code(mrb, p);
  mrb_run(mrb, mrb_proc_new(mrb, mrb->irep[n]), mrb_nil_value());
  mrb_parser_free(p);
  mrbc_context_free(mrb,mrbc_ctx);
  mrb_close(mrb);
  return 0;
}
/* vim: set ts=8 sw=2 sts=2: */
