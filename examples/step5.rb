puts 'Creating a Policy...'
p = M::Policy.new(20)
puts "Created: #{p.inspect}"
puts "Policy[1] is: #{p.getElem(1)}"
p.setElem(5,2.2)
puts "Policy[5] is: #{p.getElem(5)}"
puts "The value of your Policy is: #{p.calculateValue}"
puts "Looks like you've been swindled!"

puts("The Master Policy is: #{M::Policy::MASTER.calculateValue}")
M::Policy::MASTER.setElem(1, 9.81)
puts("The Master Policy is now: #{M::Policy::MASTER.calculateValue}")
