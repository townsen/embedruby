/*
 [step1] run ruby code.
*/

#include "mruby.h"
#include "mruby/compile.h"
#include "mruby/proc.h"

int main()
{
  mrb_state *mrb;
  int n;
  struct mrb_parser_state *p;
  mrbc_context *mrbc_ctx;

  mrb = mrb_open();
  mrbc_ctx = mrbc_context_new(mrb);
  p = mrb_parse_string(mrb, "puts 'hello'; puts 1+2; p({:a=>[1,2,3], :b=>{'c'=>{'d'=>['e', {'f'=>nil}]}}})",mrbc_ctx);
  n = mrb_generate_code(mrb, p);
  mrb_parser_free(p);
  mrbc_context_free(mrb,mrbc_ctx);
  mrb_run(mrb, mrb_proc_new(mrb, mrb->irep[n]), mrb_nil_value());
  if (mrb->exc) {
   mrb_p(mrb, mrb_obj_value(mrb->exc));
  }
  mrb_close(mrb);
  return 0; /* returns 255 otherwise */
}
/* vim: set ts=8 sw=2 sts=2: */
