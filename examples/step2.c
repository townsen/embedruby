/*
 [step2] run ruby code in file.
*/

#include "mruby.h"
#include "mruby/compile.h"
#include "mruby/proc.h"
#include <stdio.h>

void _error(const char* s){
  printf("ERROR: %s\n", s);
  exit(1);
}

int main()
{
  mrb_state *mrb;
  int n;
  mrbc_context *mrbc_ctx;
  struct mrb_parser_state *p;
  FILE* f;

  mrb = mrb_open();
  mrbc_ctx = mrbc_context_new(mrb);
  f = fopen("step2.rb", "r");
  if(f==NULL){ _error("file not found."); }
  p = mrb_parse_file(mrb,f,mrbc_ctx);
  fclose(f);
  n = mrb_generate_code(mrb, p);
  mrb_run(mrb, mrb_proc_new(mrb, mrb->irep[n]), mrb_nil_value());
  mrb_parser_free(p);
  mrbc_context_free(mrb,mrbc_ctx);
  mrb_close(mrb);
  return 0;
}
/* vim: set ts=8 sw=2 sts=2: */
