require 'rake/clean'

steps = (1..7).to_a.map{|d| "step#{d}"}

steps.delete("step5")

task :default => :build

policydir   = File.absolute_path "../Policy"

case RUBY_PLATFORM
when /mswin32/
  mrubydir= ENV['MRUBY'] || "#{ENV['HOME']}/mruby"
  EXEEXT = '.exe'
  boostdir = 'c:/usr/local/boost/boost_1_50'
  CLEAN.include('*.obj', '*.pdb', '*.ilk')
  CC = "cl -nologo -MD -EHsc -Zi -W2 -wd4996 -we4028 -we4142 -O2sy- -Zm600"
  CPPFLAGS = "-DBOOST_DATE_TIME_DYN_LINK"
  LINKOPT = %(/link -libpath:"#{mrubydir}/lib" -libpath:"#{boostdir}/lib" -libpath:"#{policydir}/lib" -incremental:no -opt:ref -opt:icf -stack:0x200000)
  OUTFLAG = '/Fe'
  LIBS = "Policy.lib mruby.lib"
  ENV['PATH'] = "#{ENV['PATH']};#{policydir}/lib"
when /linux/
  mrubydir= ENV['MRUBY'] || "/usr/local/mruby"
  EXEEXT = ''
  # Under Centos the RPM packages load boost into standard locations
  boostdir = '/usr/local/boost/boost_1_50'
  CLEAN.include('*.o')
  CC="gcc"
  CPPFLAGS = "-DBOOST_DATE_TIME_DYN_LINK"
  LINKOPT="-L #{mrubydir}/lib -L #{boostdir} -L #{policydir}/lib"
  OUTFLAG = '-o '
  LIBS = "-lm -lmruby -lboost_date_time -lPolicy" # lm is the math library, not necessary on OSX
  DEBUG="-g"
  ENV['LD_LIBRARY_PATH'] = "#{policydir}/lib"
else
  raise "Platform #{RUBY_PLATFORM} not supported!"
end

INCPATH=%(-I. -I"#{mrubydir}/include" -I"#{boostdir}" -I"#{policydir}/include")


directory 'bin'

steps.each do |s|
  target = "bin/#{s}#{EXEEXT}" 
  file target => ['bin', "#{s}.c"] do |f|
    sh "#{CC} #{CPPFLAGS} #{INCPATH} #{OUTFLAG}#{f.name} #{s}.c #{LINKOPT} #{LIBS}"
  end
  desc "Build #{s}"
  task :build => target # accumulate build task

  desc "Test #{s}"
  task s.to_sym => target do
    puts "#{s}\n"
    puts "-"*40
    sh "bin/#{s}#{EXEEXT}"
  end
  task :test => s.to_sym # accumulate test task
end

CLOBBER << steps.map{|s| "bin/#{s}#{EXEEXT}"}

# vim: set ts=8 sw=2 sts=2:
