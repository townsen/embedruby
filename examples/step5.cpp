/*
 [step5] run ruby code that uses Policy objects.
*/

#include "mruby.h"
#include "mruby/compile.h"
#include "mruby/proc.h"

#include "../Policy/include/Policy.h"

extern "C" {
  void MPolicy_Init(mrb_state *mrb, Policy *mp);
}

void _error(const char* s){
  printf("ERROR: %s\n", s);
  exit(1);
}

int main()
{
  mrb_state *mrb;
  int n;
  struct mrb_parser_state *p;
  mrbc_context *mrbc_ctx;
  FILE* f;

  // Setup the master policy to pass into Ruby
  //
  Policy *masterpol = new Policy(111);

  mrb = mrb_open();
  MPolicy_Init(mrb, masterpol);
  mrbc_ctx = mrbc_context_new(mrb);
  f = fopen("step5.rb", "r");
  if(f==NULL){ _error("file not found."); }
  p = mrb_parse_file(mrb,f,mrbc_ctx);
  fclose(f);
  n = mrb_generate_code(mrb, p);
  mrb_parser_free(p);
  mrbc_context_free(mrb,mrbc_ctx);
  mrb_run(mrb, mrb_proc_new(mrb, mrb->irep[n]), mrb_nil_value());
  if (mrb->exc) {
   mrb_p(mrb, mrb_obj_value(mrb->exc));
  }
  mrb_close(mrb);
  return 0; /* returns 255 otherwise */
}
/* vim: set ts=8 sw=2 sts=2: */
