begin
  puts "testing comparison API in C funcs"

  puts "checkme(1,1.0)"; checkme(1,1.0)
  puts "checkme(1,1)"; checkme(1,1)
  puts "checkme(1,2)"; checkme(1,2)
  puts "checkme('1',2)"; checkme('1',2)
  puts "checkme('1',1)"; checkme('1',1)
  puts "checkme([1,3], [1,3])"; checkme([1,3], [1,3])
  a = [1,3]
  b = [1,3]
  puts "checkme(a, a)"; checkme(a, a)
  puts "checkme(:a, :b)"; checkme(:a, :b)
  puts "checkme(:a, :a)"; checkme(:a, :a)
rescue => e
  p e
end

# vim: set ts=8 sw=2 sts=2:
