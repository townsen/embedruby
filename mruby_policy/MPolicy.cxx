/* Since we are going to write in a SWIG style in these simple wrappers I put
 * the necessary stuff into a separate include
 */
#include <mruby.h>
#include <mruby/array.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/hash.h>
#include <mruby/string.h>
#include <mruby/variable.h>

// The C++ object we want to wrap
//
#include "Policy.h"

#define SWIGINTERN

#ifdef __cplusplus
extern "C" {
#endif

// Use mrb for the interpreter state as MRuby macros use that
static mrb_state  *mrb = NULL;


static struct RClass *mM = NULL;
struct RClass *mPolicy = NULL;
struct RClass *kPolicy = NULL;

/* ===========================
 * Define the wrapping methods
 * =========================== */

SWIGINTERN void
free_Policy(mrb_state *mrb, void *arg1) {
  delete (Policy *)arg1; // Note this would be mrb_free(mrb,arg1) for MRuby allocs
}

static struct mrb_data_type mrb_Policy_type = { "Policy", free_Policy };

SWIGINTERN mrb_value
_wrap_new_Policy(mrb_state *mrb, mrb_value self) {
  Policy    *policy;
  int	    n, nEl;

  policy = (Policy *)mrb_get_datatype(mrb, self, &mrb_Policy_type);

  if (policy) {
      free_Policy(mrb, policy);
  }
  n = mrb_get_args(mrb, "i", &nEl);
  policy = new Policy((long)nEl);
  self = mrb_obj_value(Data_Wrap_Struct(mrb, mrb_class_ptr(self), &mrb_Policy_type, policy));
  return self;
}


SWIGINTERN mrb_value
_wrap_Policy_getElem(mrb_state *mrb, mrb_value self) {
  Policy  *policy;
  int	  n, ixEl = 0;
  double  result;

  n = mrb_get_args(mrb, "i", &ixEl);
  policy = (Policy *) mrb_get_datatype(mrb, self, &mrb_Policy_type);
  result = policy->getElem((long)ixEl);
  return mrb_float_value(result);
}


SWIGINTERN mrb_value
_wrap_Policy_setElem(mrb_state *mrb, mrb_value self) {
  Policy  *policy;
  int	  n, ixEl = 0;
  mrb_float  val = 0.0;

  n = mrb_get_args(mrb, "if", &ixEl, &val);
  policy = (Policy *) mrb_get_datatype(mrb, self, &mrb_Policy_type);
  policy->setElem(ixEl, val);
  return self;
}


SWIGINTERN mrb_value
_wrap_Policy_calculateValue(mrb_state *mrb, mrb_value self) {
  Policy  *policy;
  int	  n = 0;
  double  result;

  policy = (Policy *) mrb_get_datatype(mrb, self, &mrb_Policy_type);
  result = policy->calculateValue();
  return mrb_float_value(result);
}

/* END of Wrapping methods */


/* Initialize the MRuby Policy wrappers. Take the passed in C++ Policy and expose it
 * as the Ruby constant M::POLICY::MASTER
 */

SWIGINTERN
void MPolicy_Init(mrb_state *_mrb, Policy *masterpol) {

  if (mrb == NULL) {
    mrb = _mrb;
  }
  mM = mrb_define_module(mrb, "M");
  kPolicy = mrb_define_class_under(mrb, mM, "Policy", mrb->object_class);

  MRB_SET_INSTANCE_TT(kPolicy, MRB_TT_DATA); // Indicate that M::Policy class wraps data

  mrb_define_class_method(mrb, kPolicy, "new", _wrap_new_Policy, ARGS_REQ(1));
  mrb_define_method(mrb, kPolicy, "getElem",	_wrap_Policy_getElem, ARGS_REQ(1));
  mrb_define_method(mrb, kPolicy, "setElem",	_wrap_Policy_setElem, ARGS_REQ(2));
  mrb_define_method(mrb, kPolicy, "calculateValue", _wrap_Policy_calculateValue, ARGS_NONE());

  mrb_value mpol = mrb_obj_value(Data_Wrap_Struct(mrb, kPolicy, &mrb_Policy_type, masterpol));

  mrb_define_const (mrb, kPolicy, "MASTER", mpol);
}

#ifdef __cplusplus
}
#endif
/* vim: set ts=8 sw=2 sts=2: */
