#!/usr/bin/env ruby
#
require 'Policy'
require 'benchmark'

n = 50000
size = 100
p = Policy::Policy.new(size)
puts "I created a Policy! Now getting #{n} times...\n"
start = Time.now
(1..n).each do |i|
  p.setElem(i%size, p.getElem(i%size)+Random.rand)
end
finish = Time.now

elapsed = ((finish - start) * 1E9).to_int

puts "Value is #{p.calculateValue}"
puts "Averaged #{elapsed/n}nS per iteration"

# vim: set ts=8 sw=2 sts=2:
