puts "Tak benchmark"

def tak x, y, z
  if y >= x
    return z
  else
    return tak( tak(x-1, y, z),
                tak(y-1, z, x),
                tak(z-1, x, y))
  end
end


# Best with Ruby 1.9 on my CentOS VM 00.156345sec
# With MRuby 00.288947 :(
#
tak 24,16,8

# vim: set ts=8 sw=2 sts=2:
