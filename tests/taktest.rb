# Run on CentOS VM 
# With MRuby 00.288947 :(
#
# ruby -v: ruby 2.0.0p0 (2013-02-24 revision 39474) [x86_64-linux]
# Finished tests in 0.141771s, 7.0536 tests/s, 7.0536 assertions/s.
#
# ruby 1.9.3p374 (2013-01-15 revision 38858) [x86_64-linux]
# Finished tests in 0.176696s, 5.6594 tests/s, 5.6594 assertions/s.
#
#
require 'test/unit'

def tak x, y, z
  if y >= x
    return z
  else
    return tak( tak(x-1, y, z),
                tak(y-1, z, x),
                tak(z-1, x, y))
  end
end

class TestTak < Test::Unit::TestCase
  def test_tak
    assert_equal(9, tak(24,16,8))
  end
end

# vim: set ts=8 sw=2 sts=2:
