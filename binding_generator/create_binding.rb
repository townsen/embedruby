Types = {
  'int'   => { format: 'i', mrb_type: 'mrb_int', mrb_constructor: 'mrb_int_value' },
  'double' => { format: 'f', mrb_type: 'mrb_float', mrb_constructor: 'mrb_float_value' },
}

policy_class_def = {
  name: 'Policy',
  include_file: 'Policy.h',
  constructor: {
    args: []
  },
  methods: [
    {name: 'GetAmount', args: [{name: 'index', type: 'int'}], return_type: 'double'},
	{name: 'SetAmount', args: [{name: 'index', type: 'int'}, {name: 'value', type: 'double'}]},
  ]
}

class CppClass
  def initialize(class_def)
    @def = class_def
  end
  def declare_storage(args)
    args.map{ |arg| arg[:type] + ' ' + arg[:name] + ";" }
  end
  def format(args)
	args.map{ |arg| Types[arg[:type]][:format] }.join()
  end
  def list_by_address(args)
    args.map{ |arg| '&' + arg[:name] }.join(', ')
  end
  def list_by_name(args)
    args.map{ |arg| arg[:name] }.join(', ')
  end
  def ruby_type_constructor(type)
    Types[type][:mrb_constructor]
  end
  def specifier(args)
    if args.any?
	  "ARGS_REQ(#{args.length})"
	else
	  'ARGS_NONE()'
	end
  end
  def wrap(name)
    'wrap_' + name
  end
  def get_binding
    binding
  end
end

require 'erb'

def ApplyTemplate( class_def, template_name )
  erb = ERB.new(File.read(template_name))
  erb.result( CppClass.new(class_def).get_binding )
end

def WriteFile( filename, content )
  f = File.open( filename, 'w' )
  f.write( content )
end
  
def GenerateBinding( class_def )
  #header file
  h_content = ApplyTemplate( class_def, 'wrap_template.h' )
  WriteFile( "wrap_#{class_def[:name]}.h", h_content )
  #source file
  cpp_content = ApplyTemplate( class_def, 'wrap_template.cpp' )
  WriteFile( "wrap_#{class_def[:name]}.cpp", cpp_content )
end

GenerateBinding( policy_class_def )
