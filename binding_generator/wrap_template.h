#ifndef __WRAP_<%= @def[:name] %>_H__
#define __WRAP_<%= @def[:name] %>_H__

#include <memory>
#include "mruby.h"

class Policy;

mrb_value mrb_<%= @def[:name] %>_create(mrb_state *mrb, std::auto_ptr< <%= @def[:name] %> > obj);
void declare_<%= @def[:name] %>(mrb_state *_mrb, RClass* parent_module);

#endif // __WRAP_<%= @def[:name] %>_H__
