#include "StdAfx.h"

#include "mruby.h"
#include "mruby/data.h"
#include "mruby/value.h"
#include "mruby/string.h"
#include "mruby/variable.h"
#include "mruby/class.h"

#include "<%= @def[:include_file] %>"

#define XSTR(s) _STR(s)
#define _STR(s) #s
#define OBJECT_TYPE <%= @def[:name] %>

static struct RClass* ruby_class = 0;

static void free_Object(mrb_state *mrb, void *obj) 
{
	delete (OBJECT_TYPE *)obj;
}

static struct mrb_data_type mrb_object_infos_type = { XSTR(OBJECT_TYPE), free_Object };


static mrb_value wrap_create(mrb_state *mrb, mrb_value self) 
{
	// create a new ruby instance of ruby_class that wraps a newly instantiated C++ OBJECT_TYPE
	<% if @def[:constructor][:args].any? %>
	<%= declare_storage(@def[:constructor][:args]) %>
	mrb_get_args(
		mrb,
		"<%= format(@def[:constructor][:args]) %>",
		<%= list_by_address(@def[:constructor][:args]) %>
		);
	<% end %>
	return mrb_obj_value(Data_Wrap_Struct(mrb, ruby_class, &mrb_object_infos_type, new OBJECT_TYPE(<%= list_by_name(@def[:constructor][:args]) %>)));
}

/** this method transfers pointers ownership to ruby */
mrb_value mrb_<%= @def[:name] %>_create(mrb_state *mrb, std::auto_ptr<OBJECT_TYPE> obj)
{
	// be carefull, pointer life management now relies on ruby
	return mrb_obj_value( Data_Wrap_Struct( mrb, ruby_class, &mrb_object_infos_type, obj.release() ) );
}

<% @def[:methods].each do |m| %>
static mrb_value <%= wrap(m[:name]) %>(mrb_state *mrb, mrb_value self) 
{
	// retrieve c++ pointer from attribute
	OBJECT_TYPE* c_object = (OBJECT_TYPE *) mrb_get_datatype(mrb, self, &mrb_object_infos_type);
	<% if m[:args].any? %>
	<% declare_storage(m[:args]).each do |arg_storage_declaration| %>
	<%= arg_storage_declaration %>
	<% end %>
	mrb_get_args(
		mrb,
		"<%= format(m[:args]) %>",
		<%= list_by_address(m[:args]) %>
		);
	<% end %>
	<% if m[:return_type] != nil %>
	return <%= ruby_type_constructor(m[:return_type]) %>( c_object-><%= m[:name] %>(<%= list_by_name(m[:args]) %>) );
	<% else %>
	c_object-><%= m[:name] %>(<%= list_by_name(m[:args]) %>);
	return mrb_nil_value();
	<% end %>
}
<% end %>

static void declare_class(mrb_state *_mrb, RClass* parent_module) 
{
	// Moodys::Policy class	
	ruby_class = mrb_define_class_under(_mrb, parent_module, XSTR(OBJECT_TYPE), _mrb->object_class);
	mrb_define_singleton_method(_mrb, (RObject*)ruby_class, "new", wrap_create, <%= specifier(@def[:constructor][:args]) %>  );
	<% @def[:methods].each do |m| %>
	mrb_define_method( _mrb, ruby_class, "<%= m[:name] %>", <%= wrap(m[:name]) %>, <%= specifier(m[:args]) %> );
	<% end %>
}

void declare_<%= @def[:name] %>(mrb_state *_mrb, RClass* parent_module)
{
	declare_class(_mrb, parent_module);
}

