#include <iostream>
#include <ctime>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "include/Policy.h"

using namespace std;
using namespace boost::posix_time;

int count = 100000;

int main(int argc, char* argv[]) {
    if (argc > 1) {
	::count = atol(argv[1]);
    }
    cout << "Iterating " << ::count << " times" << endl;
    ptime start = microsec_clock::local_time();
    Policy *p = new Policy(100L);
    for (int i = 0; i < ::count; i++) {
	p->setElem(1, 3.13);
	double val = p->getElem(1);
    }
    ptime end = microsec_clock::local_time();
    time_duration td = end - start;
    long nanosperiter = td.total_nanoseconds() / ::count;
    cout << "Timed at "<< td << endl;
    cout << "Each iteration took "<< nanosperiter << " nanoseconds" << endl;
}
// vim: set ts=8 sw=4 sts=4:
