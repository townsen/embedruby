#pragma once

#include <vector>

#ifndef _Policy_DLLDEFINES_H_
#define _Policy_DLLDEFINES_H_

/* Cmake will define Policy_EXPORTS on Windows when it
configures to build a shared library. If you are going to use
another build system on windows or create the visual studio
projects by hand you need to define Policy_EXPORTS when
building a DLL on windows.
*/
// We are using the Visual Studio Compiler and building Shared libraries

#if defined (_WIN32)
  #if defined(Policy_EXPORTS)
    #define  POLICY_EXPORT __declspec(dllexport)
  #else
    #define  POLICY_EXPORT __declspec(dllimport)
  #endif /* Policy_EXPORTS */
#else /* defined (_WIN32) */
 #define POLICY_EXPORT
#endif

#endif /* _Policy_DLLDEFINES_H_ */


// Disable warning 4251 on exported vector
// See http://www.unknownroad.com/rtfm/VisualStudio/warningC4251.html
//
#ifdef _WIN32
#  pragma warning( push )
#  pragma warning( disable: 4251 )
#endif


class POLICY_EXPORT Policy
{
public:
    Policy(long nbElem);
    ~Policy();

    double getElem(long index);

    long setElem(long index, double value);

    double	calculateValue();

private:
    std::vector<double> vectPol;
};

#ifdef _WIN32
#  pragma warning( pop )
#endif

// vim: set ts=8 sw=4 sts=4:
