#include "include/Policy.h"
#include <vector>
#include <numeric>

Policy::Policy(long nbElem)
{
  for (long i=0; i<nbElem; i++) {
    vectPol.push_back(i);
  }
}

Policy::~Policy(void)
{
}

double Policy::getElem(long index)
{
  if (index>=vectPol.size())
  {
    return 0.;
  }
  return vectPol[index];
}

long Policy::setElem(long index, double value)
{
  if (index>=vectPol.size())
  {
    return -1;
  }
  vectPol[index] =  value;
  return 0;
}
double Policy::calculateValue(void) {
  double tot = 0.0;
  std::vector<double>::iterator it;
  for ( it=vectPol.begin() ; it < vectPol.end(); it++ ) tot += *it;
  tot = std::accumulate(vectPol.begin(),vectPol.end(),0.0);
  return tot;
}
/* vim: set ts=8 sw=2 sts=2: */
