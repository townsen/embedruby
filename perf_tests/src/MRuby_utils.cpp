#include "MRuby_utils.h"

#include <mruby/compile.h>
#include <stdio.h>

static void file_not_found(const char* s){
	printf("ERROR: file not found: %s\n", s);
	exit(1);
}

int mruby_load_script_from_file( mrb_state& mrb, const char* file_name )
{
	FILE* f;
	struct mrb_parser_state *p;
	fopen_s(&f, file_name, "r");
	if(f==NULL){ file_not_found(file_name); }
	mrbc_context *mrbc_ctx = mrbc_context_new(&mrb);
	p = mrb_parse_file(&mrb,f,mrbc_ctx);
	fclose(f);
	int n = mrb_generate_code(&mrb, p);
	mrb_parser_free(p);
	mrbc_context_free(&mrb,mrbc_ctx);
	return n;
}

int mruby_load_script_from_string( mrb_state& mrb, const char* script )
{
	struct mrb_parser_state *p;
	mrbc_context *mrbc_ctx = mrbc_context_new(&mrb);
	p = mrb_parse_string(&mrb,script,mrbc_ctx);
	int n = mrb_generate_code(&mrb, p);
	mrb_parser_free(p);
	mrbc_context_free(&mrb,mrbc_ctx);
	return n;
}
