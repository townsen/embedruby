#include "test_PolicyAccess.h"
#include "BenchmarkTest.h"
#include "MRuby_utils.h"

#include "Policy.h"
#include "Policy_mruby_wrap.h"

#include <mruby.h>
#include <mruby/proc.h>


/*
struct MemStatusPrinter {
	MemStatusPrinter() : m_lastPrint(microsec_clock::local_time()), m_periodMS(200) {}
	void print() {
		ptime current_time = microsec_clock::local_time();
		time_duration td = current_time - m_lastPrint;
		if ( td.total_nanoseconds() > m_periodMS*1e6 ) {
			cout << " Processing policies: " 
				 << Policy::nb_policy_new << " created, "
				 << Policy::nb_policy_delete << " deleted, "
				 << Policy::nb_policy_new - Policy::nb_policy_delete << " in memory" << std::endl;
			m_lastPrint = current_time;
		}
	}
	ptime m_lastPrint;
	long m_periodMS;
};
*/


class PolicyAccessMRubyTest : public BenchmarkTest {
public:
	PolicyAccessMRubyTest(long nb_loop) : BenchmarkTest("MRuby Policy", nb_loop) {}

private:
	virtual void Init() {
		// reset counters
		Policy::nb_policy_new = 0;
		Policy::nb_policy_delete = 0;

		// Open mruby context
		mrb = mrb_open();

		// Binding ruby
		struct RClass *moodys_module = mrb_define_module(mrb, "Moodys");
		declare_Policy(mrb, moodys_module);

		int script_index = mruby_load_script_from_string(*mrb, 
			"p = Moodys::Policy.new()\n"
			"p.SetAmount(5, 2.34)\n"
			"ref = p.GetAmount(5)\n"
		);

		script = mrb_proc_new(mrb, mrb->irep[script_index]);
	}

	virtual void PerformIteration() 
	{
		mrb_run(mrb, script, mrb_nil_value());
		if (mrb->exc) {
			mrb_p(mrb, mrb_obj_value(mrb->exc));
		}
	}

	virtual void Terminate()
	{
		mrb_close(mrb);
		std::cout << "* policy (" << Policy::nb_policy_new << " created, " << Policy::nb_policy_delete << " deleted)" << std::endl;
	}

	mrb_state *mrb;
	RProc* script;
};

class PolicyAccessCPPTest : public BenchmarkTest {
public:
	PolicyAccessCPPTest(long nb_loop) : BenchmarkTest("CPP Policy", nb_loop) {}

private:
	virtual void Init() {
		// reset counters
		Policy::nb_policy_new = 0;
		Policy::nb_policy_delete = 0;
	}

	virtual void PerformIteration() 
	{
		Policy p;
		p.SetAmount(5, 2.34);
		p.GetAmount(5); 
	}

	virtual void Terminate()
	{
		std::cout << "* policy (" << Policy::nb_policy_new << " created, " << Policy::nb_policy_delete << " deleted)" << std::endl;
	}
};

void test_PolicyAccess()
{
	static const long nb_loop = 1000000;
	PolicyAccessCPPTest l_cppTest(nb_loop);
	l_cppTest.Start();

	PolicyAccessMRubyTest l_rubyTest(nb_loop);
	l_rubyTest.Start();
}

