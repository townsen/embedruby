#include "test_FunctionCall.h"


#include "test_PolicyAccess.h"
#include "BenchmarkTest.h"
#include "MRuby_utils.h"

#include "Policy.h"
#include "Policy_mruby_wrap.h"

#include <mruby.h>
#include <mruby/proc.h>
#include <mruby/value.h>
#include <mruby/variable.h>

#include <boost/scoped_ptr.hpp>


class FunctionCallMRubyTest : public BenchmarkTest {
public:
	FunctionCallMRubyTest(long nb_loop) : BenchmarkTest("MRuby function call", nb_loop) {}

private:
	virtual void Init() {
		// reset counters
		Policy::nb_policy_new = 0;
		Policy::nb_policy_delete = 0;

		// Open mruby context
		mrb = mrb_open();

		// Binding ruby
		struct RClass *moodys_module = mrb_define_module(mrb, "Moodys");
		declare_Policy(mrb, moodys_module);

		int script_index = mruby_load_script_from_string(*mrb, 
			"def call_policy(policy)\n"
			"  policy.SetAmount(5, 2.34)\n"
			"  2*policy.GetAmount(5)\n"
			"end\n"
		);
		RProc* script = mrb_proc_new(mrb, mrb->irep[script_index]);
		mrb_run(mrb, script, mrb_nil_value());
		if (mrb->exc) {
			mrb_p(mrb, mrb_obj_value(mrb->exc));
		}
		func = mrb_intern(mrb, "call_policy");
		top_self = mrb_top_self(mrb);

		policy = mrb_Policy_create(mrb, std::auto_ptr<Policy>(new Policy) );
	}

	virtual void PerformIteration() 
	{
		mrb_value val = mrb_funcall_argv( mrb, top_self, func, 1, &policy );
		if (mrb->exc) {
			mrb_p(mrb, mrb_obj_value(mrb->exc));
		}
	}

	virtual void Terminate()
	{
		mrb_close(mrb);
		std::cout << "* policy (" << Policy::nb_policy_new << " created, " << Policy::nb_policy_delete << " deleted)" << std::endl;
	}

	mrb_state *mrb;
	mrb_value policy;
	mrb_value top_self;
	mrb_sym func;
};

static double Call_Func(Policy& policy)
{
	policy.SetAmount(5, 2.34);
	return 2*policy.GetAmount(5);
}

class FunctionCallCPPTest : public BenchmarkTest {
public:
	FunctionCallCPPTest(long nb_loop) : BenchmarkTest("CPP function call", nb_loop) {}

private:
	virtual void Init() {
		// reset counters
		Policy::nb_policy_new = 0;
		Policy::nb_policy_delete = 0;
		policy.reset( new Policy );
	}

	virtual void PerformIteration() 
	{
		Call_Func(*policy);
	}

	virtual void Terminate()
	{
		policy.reset();
		std::cout << "* policy (" << Policy::nb_policy_new << " created, " << Policy::nb_policy_delete << " deleted)" << std::endl;
	}

	boost::scoped_ptr<Policy> policy;

};

void test_FunctionCall()
{
	static const long nb_loop = 1000000;
	FunctionCallCPPTest l_cppTest(nb_loop);
	l_cppTest.Start();

	FunctionCallMRubyTest l_rubyTest(nb_loop);
	l_rubyTest.Start();
}

