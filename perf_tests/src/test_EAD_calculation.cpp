#include "test_EAD_calculation.h"
#include "BenchmarkTest.h"
#include "MRuby_utils.h"

#include "EAD_Infos.h"
#include "EAD_infos_own_wrap.h"
#include "EAD_infos_ruby_wrap.h"
#include "EAD_infos_python_wrap.h"

#include <mruby.h>
#include <mruby/proc.h>
#include <mruby/value.h>
#include <mruby/variable.h>

#include <boost/scoped_ptr.hpp>





void init_EAD_data(EAD_infos& ead_info)
{
	ead_info.original_maturity = 500.0;
	ead_info.ead_drawn = 999.9;
	ead_info.residual_maturity = 111.1;
	ead_info.fair_value = 1000.0;
	ead_info.accrued_interests = 256.78;
	ead_info.past_due_int = 253.6;
	ead_info.past_due_ppal = 745.5;
	ead_info.accrued_fees = 211.6;
	ead_info.accrued_interests = 522.9;
	ead_info.draw_proba = 0.86;
}


class EADCalculationMRubyTest : public BenchmarkTest {
public:
	EADCalculationMRubyTest(long nb_loop) : BenchmarkTest("MRuby EAD calculation", nb_loop) {}

private:
	virtual void Init() {

		// Init C++ datas
		init_EAD_data(ead_infos);

		// Open mruby context
		mrb = mrb_open();

		// Binding ruby
		Init_EAD_infos(mrb,&ead_infos);

		int script_index = mruby_load_script_from_string(*mrb, 
														 "Moodys::EAD_infos::EAD_INFOS.ead_calc = (Moodys::EAD_infos::EAD_INFOS.fair_value + "
														 "Moodys::EAD_infos::EAD_INFOS.past_due_int + Moodys::EAD_infos::EAD_INFOS.past_due_ppal + "
														 "Moodys::EAD_infos::EAD_INFOS.accrued_fees + Moodys::EAD_infos::EAD_INFOS.accrued_interests) * "
														 "Moodys::EAD_infos::EAD_INFOS.draw_proba");




		script = mrb_proc_new(mrb, mrb->irep[script_index]);

	}

	virtual void PerformIteration() 
	{
		ead_infos.fair_value += 1.0;
		mrb_run(mrb, script, mrb_nil_value());
		if (mrb->exc) {
			mrb_p(mrb, mrb_obj_value(mrb->exc));
		}
	}

	virtual void Terminate()
	{
		mrb_close(mrb);
		std::cout << "* " << nb_loop <<  " EAD calculated"  << std::endl;
		std::cout << "* Last EAD calculated: " << ead_infos.ead_calc  << std::endl;
	}


	mrb_state *mrb;
	RProc * script;
	EAD_infos ead_infos;
};


class EADCalculationCPPTest : public BenchmarkTest {
public:
	EADCalculationCPPTest(long nb_loop) : BenchmarkTest("CPP EAD calculation", nb_loop) {}

private:
	virtual void Init() 
	{
		// Init C++ datas
		init_EAD_data(ead_infos);
	}

	virtual void PerformIteration() 
	{
		ead_infos.fair_value += 1.0;
		//test.ead_calc = test.fair_value + test.original_maturity;
		ead_infos.ead_calc = (ead_infos.fair_value + ead_infos.past_due_int + ead_infos.past_due_ppal + ead_infos.accrued_fees + ead_infos.accrued_interests) * ead_infos.draw_proba;
	}

	virtual void Terminate()
	{
		std::cout << "* " << nb_loop <<  " EAD calculated"  << std::endl;
		std::cout << "* Last EAD calculated: " << ead_infos.ead_calc  << std::endl;
	}

	EAD_infos ead_infos;
};



void test_EAD_calculation()
{
	static const long nb_loop = 1000000;
	EADCalculationCPPTest l_cppTest(nb_loop);
	l_cppTest.Start();

	EADCalculationMRubyTest l_rubyTest(nb_loop);
	l_rubyTest.Start();

}



