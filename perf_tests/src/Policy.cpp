#include "Policy.h"

long Policy::nb_policy_new = 0;
long Policy::nb_policy_delete = 0;

Policy::Policy() : m_reference("no reference")
{
	++nb_policy_new;
}

const std::string& Policy::GetReference() const
{
	return m_reference;
}

Policy::~Policy()
{
	++nb_policy_delete;
}

double& Policy::GetAmount( int index )
{
	if (index+1 > m_amounts.size()) {
		m_amounts.resize(index+1);
	}
	return m_amounts[index];
}

void Policy::SetAmount( int index, double value )
{
	GetAmount(index) = value;
}
