#include "test_PolicyAccess.h"
#include "test_FunctionCall.h"
#include "test_EAD_calculation.h"




int main(int argc, char* argv[])
{
	// benchmark EAD
	test_EAD_calculation();

	// benchmark object new/delete and method access
	test_FunctionCall();
	test_PolicyAccess();

	return 0; 
}

