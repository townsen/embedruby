#include "BenchmarkTest.h"

#include <boost/function/function0.hpp>
#include <boost/bind.hpp>

#include <string>
#include <iostream>

using namespace boost::posix_time;

BenchmarkTest::BenchmarkTest( const std::string& _title, const long _nb_loop) : title(_title), nb_loop(_nb_loop)
{

}

time_duration BenchmarkTest::MeasureFunctionDuration( boost::function0<void> func )
{
	
	ptime start = microsec_clock::local_time();
	func();
	ptime end = microsec_clock::local_time();
	time_duration td = end - start;
	return td; 
}

void BenchmarkTest::Iterate()
{
	for (int i=0; i<nb_loop; ++i)
	{
		PerformIteration();
	}
}

void BenchmarkTest::Start()
{
	std::cout << "************************************************" << std::endl;
	std::cout << "*            " << title << "            " << std::endl;
	std::cout << "************************************************" << std::endl;
	std::cout << "*" << std::endl;
	std::cout << "* Number of iterations: "<< nb_loop << std::endl;
	
	
	time_duration td = MeasureFunctionDuration( boost::bind(&BenchmarkTest::Init, this) );
	std::cout << "* Init performed in " << td.total_nanoseconds() << " ns" << std::endl;

	td = MeasureFunctionDuration( boost::bind(&BenchmarkTest::Iterate, this) );
	std::cout << "* All loops performed in " << td << " ms" << std::endl;
	std::cout << "* Each loop took " << td.total_nanoseconds() / nb_loop << " ns" << std::endl;
	
	td = MeasureFunctionDuration( boost::bind(&BenchmarkTest::Terminate, this) );
	std::cout << "* Terminate performed in " << td.total_nanoseconds() << " ns" << std::endl;

	std::cout << std::endl;
	std::cout << std::endl;
}
