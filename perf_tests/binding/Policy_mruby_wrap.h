#ifndef __WRAP_Policy_H__
#define __WRAP_Policy_H__

#include <memory>
#include "mruby.h"

class Policy;

mrb_value mrb_Policy_create(mrb_state *mrb, std::auto_ptr< Policy > obj);
void declare_Policy(mrb_state *_mrb, RClass* parent_module);

#endif // __WRAP_Policy_H__