#ifdef _WIN64

#include <mruby.h>
#include <mruby/array.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/hash.h>
#include <mruby/string.h>
#include <mruby/variable.h>

#include "EAD_infos.h"


// Use mrb for the interpreter state as MRuby macros use that
static mrb_state  *mrb = NULL;


static struct RClass *mM = NULL;
struct RClass *mPolicy = NULL;
struct RClass *kPolicy = NULL;


#ifdef __cplusplus
extern "C"
#endif

void
free_EAD_Infos(mrb_state *mrb, void *arg1) 
{
  //delete (EAD_infos *)arg1; // Note this would be mrb_free(mrb,arg1) for MRuby allocs
}

static struct mrb_data_type mrb_EAD_infos_type = { "EAD_infos", free_EAD_Infos };



mrb_value
_wrap_EAD_infos_ead_calc_set(mrb_state *mrb, mrb_value self) 
{
	mrb_float  ead = 0.0;
	mrb_get_args(mrb, "f", &ead);
	((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->ead_calc = ead;
	return self;
}


mrb_value
_wrap_EAD_infos_original_maturity_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->original_maturity);
}


mrb_value
_wrap_EAD_infos_ead_drawn_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->ead_drawn);
}


mrb_value
_wrap_EAD_infos_residual_maturity_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->residual_maturity);
}


mrb_value
_wrap_EAD_infos_fair_value_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->fair_value);
}

mrb_value
_wrap_EAD_infos_accrued_interests_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->accrued_interests);
}

mrb_value
_wrap_EAD_infos_past_due_int_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->past_due_int);
}

mrb_value
_wrap_EAD_infos_past_due_ppal_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->past_due_ppal);
}

mrb_value
_wrap_EAD_infos_accrued_fees_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->accrued_fees);
}

mrb_value
_wrap_EAD_infos_draw_proba_get(mrb_state *mrb, mrb_value self) 
{
  return mrb_float_value(((EAD_infos *) mrb_get_datatype(mrb, self, &mrb_EAD_infos_type))->draw_proba);
}






void Init_EAD_infos(mrb_state *_mrb, EAD_infos *ead_infos) 
{
	struct RClass *moodys_module = NULL;
	struct RClass *ead_class = NULL;

	moodys_module = mrb_define_module(_mrb, "Moodys");
	ead_class = mrb_define_class_under(_mrb, moodys_module, "EAD_infos", _mrb->object_class);

	MRB_SET_INSTANCE_TT(ead_class, MRB_TT_DATA); // Indicate that Moodys::EAD_infos class wraps data

	mrb_define_method(_mrb, ead_class, "ead_calc=",	_wrap_EAD_infos_ead_calc_set,  ARGS_REQ(1));

	mrb_define_method(_mrb, ead_class, "original_maturity",	_wrap_EAD_infos_original_maturity_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "ead_drawn", _wrap_EAD_infos_ead_drawn_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "residual_maturity", _wrap_EAD_infos_residual_maturity_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "fair_value", _wrap_EAD_infos_fair_value_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "accrued_interests", _wrap_EAD_infos_accrued_interests_get, ARGS_NONE());

	mrb_define_method(_mrb, ead_class, "past_due_int", _wrap_EAD_infos_past_due_int_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "past_due_ppal", _wrap_EAD_infos_past_due_ppal_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "accrued_fees", _wrap_EAD_infos_accrued_fees_get, ARGS_NONE());
	mrb_define_method(_mrb, ead_class, "draw_proba", _wrap_EAD_infos_draw_proba_get, ARGS_NONE());


	mrb_value ruby_ead_infos = mrb_obj_value(Data_Wrap_Struct(_mrb, ead_class, &mrb_EAD_infos_type, ead_infos));

	mrb_define_const (_mrb, ead_class, "EAD_INFOS", ruby_ead_infos);

}

#endif 
/*
  mrb_define_method(mrb, ead_class, "past_due_ppal=", _wrap_EAD_infos_past_due_ppal_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "past_due_ppal", _wrap_EAD_infos_past_due_ppal_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "past_due_int=", _wrap_EAD_infos_past_due_int_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "past_due_int", _wrap_EAD_infos_past_due_int_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amortized_cost=", _wrap_EAD_infos_amortized_cost_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amortized_cost", _wrap_EAD_infos_amortized_cost_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "outstanding=", _wrap_EAD_infos_outstanding_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "outstanding", _wrap_EAD_infos_outstanding_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "accrued_fees=", _wrap_EAD_infos_accrued_fees_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "accrued_fees", _wrap_EAD_infos_accrued_fees_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "nominal=", _wrap_EAD_infos_nominal_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "nominal", _wrap_EAD_infos_nominal_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "net_interest_income=", _wrap_EAD_infos_net_interest_income_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "net_interest_income", _wrap_EAD_infos_net_interest_income_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "original_cost=", _wrap_EAD_infos_original_cost_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "original_cost", _wrap_EAD_infos_original_cost_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amortized_cost_no_ias=", _wrap_EAD_infos_amortized_cost_no_ias_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amortized_cost_no_ias", _wrap_EAD_infos_amortized_cost_no_ias_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "ccf=", _wrap_EAD_infos_ccf_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "ccf", _wrap_EAD_infos_ccf_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "add_on=", _wrap_EAD_infos_add_on_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "add_on", _wrap_EAD_infos_add_on_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "draw_proba=", _wrap_EAD_infos_draw_proba_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "draw_proba", _wrap_EAD_infos_draw_proba_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "available_facility=", _wrap_EAD_infos_available_facility_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "available_facility", _wrap_EAD_infos_available_facility_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "mark_to_model_amount=", _wrap_EAD_infos_mark_to_model_amount_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "mark_to_model_amount", _wrap_EAD_infos_mark_to_model_amount_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "external_deal_imported_ead=", _wrap_EAD_infos_external_deal_imported_ead_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "external_deal_imported_ead", _wrap_EAD_infos_external_deal_imported_ead_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "small_tickets_imported_ead=", _wrap_EAD_infos_small_tickets_imported_ead_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "small_tickets_imported_ead", _wrap_EAD_infos_small_tickets_imported_ead_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_investors_interest_others=", _wrap_EAD_infos_secu_investors_interest_others_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_investors_interest_others", _wrap_EAD_infos_secu_investors_interest_others_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_ccf_others=", _wrap_EAD_infos_secu_ccf_others_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_ccf_others", _wrap_EAD_infos_secu_ccf_others_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_inversors_interest_uncommited_retail=", _wrap_EAD_infos_secu_inversors_interest_uncommited_retail_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_inversors_interest_uncommited_retail", _wrap_EAD_infos_secu_inversors_interest_uncommited_retail_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_ccf_uncommited_retail=", _wrap_EAD_infos_secu_ccf_uncommited_retail_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "secu_ccf_uncommited_retail", _wrap_EAD_infos_secu_ccf_uncommited_retail_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_sset_security_nominal=", _wrap_EAD_infos_unsettled_sset_security_nominal_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_sset_security_nominal", _wrap_EAD_infos_unsettled_sset_security_nominal_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_bid_price=", _wrap_EAD_infos_unsettled_security_bid_price_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_bid_price", _wrap_EAD_infos_unsettled_security_bid_price_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_price_factor=", _wrap_EAD_infos_unsettled_security_price_factor_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_price_factor", _wrap_EAD_infos_unsettled_security_price_factor_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_dap_security_nominal=", _wrap_EAD_infos_unsettled_dap_security_nominal_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_dap_security_nominal", _wrap_EAD_infos_unsettled_dap_security_nominal_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_dap_transaction_clean_price=", _wrap_EAD_infos_unsettled_dap_transaction_clean_price_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_dap_transaction_clean_price", _wrap_EAD_infos_unsettled_dap_transaction_clean_price_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_offer_price=", _wrap_EAD_infos_unsettled_security_offer_price_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_security_offer_price", _wrap_EAD_infos_unsettled_security_offer_price_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "leasing_residual_value=", _wrap_EAD_infos_leasing_residual_value_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "leasing_residual_value", _wrap_EAD_infos_leasing_residual_value_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "hfs=", _wrap_EAD_infos_hfs_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "hfs", _wrap_EAD_infos_hfs_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "leasing_bargain_option_value=", _wrap_EAD_infos_leasing_bargain_option_value_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "leasing_bargain_option_value", _wrap_EAD_infos_leasing_bargain_option_value_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_agreed_settlement_value=", _wrap_EAD_infos_unsettled_agreed_settlement_value_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_agreed_settlement_value", _wrap_EAD_infos_unsettled_agreed_settlement_value_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_underlying_market_value=", _wrap_EAD_infos_unsettled_underlying_market_value_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "unsettled_underlying_market_value", _wrap_EAD_infos_unsettled_underlying_market_value_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_imported_ead=", _wrap_EAD_infos_retail_imported_ead_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_imported_ead", _wrap_EAD_infos_retail_imported_ead_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "premium_current=", _wrap_EAD_infos_premium_current_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "premium_current", _wrap_EAD_infos_premium_current_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_pool_attribute_1=", _wrap_EAD_infos_retail_pool_attribute_1_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_pool_attribute_1", _wrap_EAD_infos_retail_pool_attribute_1_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_pool_attribute_2=", _wrap_EAD_infos_retail_pool_attribute_2_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_pool_attribute_2", _wrap_EAD_infos_retail_pool_attribute_2_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_exposure_attribute_1=", _wrap_EAD_infos_retail_exposure_attribute_1_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_exposure_attribute_1", _wrap_EAD_infos_retail_exposure_attribute_1_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_exposure_attribute_3=", _wrap_EAD_infos_retail_exposure_attribute_3_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "retail_exposure_attribute_3", _wrap_EAD_infos_retail_exposure_attribute_3_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "effective_epe=", _wrap_EAD_infos_effective_epe_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "effective_epe", _wrap_EAD_infos_effective_epe_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "delta=", _wrap_EAD_infos_delta_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "delta", _wrap_EAD_infos_delta_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount1=", _wrap_EAD_infos_amount1_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount1", _wrap_EAD_infos_amount1_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount2=", _wrap_EAD_infos_amount2_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount2", _wrap_EAD_infos_amount2_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "hj=", _wrap_EAD_infos_hj_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "hj", _wrap_EAD_infos_hj_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "acquisition_cost=", _wrap_EAD_infos_acquisition_cost_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "acquisition_cost", _wrap_EAD_infos_acquisition_cost_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "underlying_market_value=", _wrap_EAD_infos_underlying_market_value_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "underlying_market_value", _wrap_EAD_infos_underlying_market_value_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "lossesCausedByPriceVariation=", _wrap_EAD_infos_lossesCausedByPriceVariation_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "lossesCausedByPriceVariation", _wrap_EAD_infos_lossesCausedByPriceVariation_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "agreedAmount=", _wrap_EAD_infos_agreedAmount_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "agreedAmount", _wrap_EAD_infos_agreedAmount_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "strike=", _wrap_EAD_infos_strike_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "strike", _wrap_EAD_infos_strike_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "nominalPosition=", _wrap_EAD_infos_nominalPosition_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "nominalPosition", _wrap_EAD_infos_nominalPosition_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "attribute_9=", _wrap_EAD_infos_attribute_9_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "attribute_9", _wrap_EAD_infos_attribute_9_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount_1=", _wrap_EAD_infos_amount_1_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount_1", _wrap_EAD_infos_amount_1_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount_2=", _wrap_EAD_infos_amount_2_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "amount_2", _wrap_EAD_infos_amount_2_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "exchange_rate=", _wrap_EAD_infos_exchange_rate_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "exchange_rate", _wrap_EAD_infos_exchange_rate_get, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "ccp_ead_scalar=", _wrap_EAD_infos_ccp_ead_scalar_set, ARGS_NONE());
  mrb_define_method(mrb, ead_class, "ccp_ead_scalar", _wrap_EAD_infos_ccp_ead_scalar_get, ARGS_NONE());
  SwigClassEAD_infos.mark = 0;
  SwigClassEAD_infos.destroy = (void (*)(void *)) free_EAD_infos;
  SwigClassEAD_infos.trackObjects = 0;

}
  */
