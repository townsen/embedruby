#include "mruby.h"
#include "mruby/data.h"
#include "mruby/value.h"
#include "mruby/string.h"
#include "mruby/variable.h"
#include "mruby/class.h"

#include "Policy.h"

#define XSTR(s) _STR(s)
#define _STR(s) #s
#define OBJECT_TYPE Policy

static struct RClass* ruby_class = 0;

static void free_Object(mrb_state *mrb, void *obj)
{
        delete (OBJECT_TYPE *)obj;
}

static struct mrb_data_type mrb_object_infos_type = { XSTR(OBJECT_TYPE), free_Object };


static mrb_value wrap_create(mrb_state *mrb, mrb_value self)
{
        // create a new ruby instance of ruby_class that wraps a newly instantiated C++ OBJECT_TYPE

        return mrb_obj_value(Data_Wrap_Struct(mrb, ruby_class, &mrb_object_infos_type, new OBJECT_TYPE()));
}

/** this method transfers pointers ownership to ruby */
mrb_value mrb_Policy_create(mrb_state *mrb, std::auto_ptr<OBJECT_TYPE> obj)
{
        // be carefull, pointer life management now relies on ruby
        return mrb_obj_value( Data_Wrap_Struct( mrb, ruby_class, &mrb_object_infos_type, obj.release() ) );
}


static mrb_value wrap_GetAmount(mrb_state *mrb, mrb_value self)
{
        // retrieve c++ pointer from attribute
        OBJECT_TYPE* c_object = (OBJECT_TYPE *) mrb_get_datatype(mrb, self, &mrb_object_infos_type);


        int index;

        mrb_get_args(
                mrb,
                "i",
                &index
                );


        return mrb_float_value( c_object->GetAmount(index) );

}

static mrb_value wrap_SetAmount(mrb_state *mrb, mrb_value self)
{
        // retrieve c++ pointer from attribute
        OBJECT_TYPE* c_object = (OBJECT_TYPE *) mrb_get_datatype(mrb, self, &mrb_object_infos_type);


        int index;

        double value;

        mrb_get_args(
                mrb,
                "if",
                &index, &value
                );


        c_object->SetAmount(index, value);
        return mrb_nil_value();

}


static void declare_class(mrb_state *_mrb, RClass* parent_module)
{
        // Moodys::Policy class
        ruby_class = mrb_define_class_under(_mrb, parent_module, XSTR(OBJECT_TYPE), _mrb->object_class);
        mrb_define_singleton_method(_mrb, (RObject*)ruby_class, "new", wrap_create, ARGS_NONE()  );

        mrb_define_method( _mrb, ruby_class, "GetAmount", wrap_GetAmount, ARGS_REQ(1) );

        mrb_define_method( _mrb, ruby_class, "SetAmount", wrap_SetAmount, ARGS_REQ(2) );

}

void declare_Policy(mrb_state *_mrb, RClass* parent_module)
{
        declare_class(_mrb, parent_module);
}
