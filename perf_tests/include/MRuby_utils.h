#ifndef __MRUBY_UTILS_H__
#define __MRUBY_UTILS_H__

struct mrb_state;

int mruby_load_script_from_file(mrb_state& mrb, const char* file_name);

int mruby_load_script_from_string(mrb_state& mrb, const char* script);

#endif //__MRUBY_UTILS_H__
