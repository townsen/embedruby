#ifndef __BENCHMARK_UTILS_H__
#define __BENCHMARK_UTILS_H__

#include <string>
#include <boost/function/function_fwd.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class BenchmarkTest {
public:
	BenchmarkTest(const std::string& _title, const long _nb_loop);
	virtual ~BenchmarkTest() {}

	void Start();

	virtual void Init() {};
	virtual void PerformIteration() = 0;
	virtual void Terminate() {};

	void Iterate();
	
	static boost::posix_time::time_duration MeasureFunctionDuration(boost::function0<void> func);

protected:
	std::string title;
	long nb_loop;
};


#endif //__BENCHMARK_UTILS_H__
