//=============================================================================================
/**
	Copyright 2005 � Fermat. All Rights Reserved.<BR>
	Project		<B>SII</B>

	@file		Policy.h

	@author		St�phane CARBO (SCA)

	<B>Overview: </B><BR>
	Interface to manipulate a policy from custom development


*/
//=============================================================================================
#if !defined(_SII_API_POLICY_H_)
#define _SII_API_POLICY_H_

#include <string>
#include <vector>



class Policy
{
public:
	///Constructor
	Policy();

	///Destructor
	~Policy();


	const std::string& GetReference() const;

	double& GetAmount(int index);

	void SetAmount(int index, double value);


	static long nb_policy_new;
	static long nb_policy_delete;
	
private:
	std::string m_reference;
	std::vector<std::string> m_attributes;
	std::vector<double> m_amounts;
};

#endif  //#if !defined(_SII_API_POLICY_H_)

