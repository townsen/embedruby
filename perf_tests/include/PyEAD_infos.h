#pragma once

#ifndef NO_PYTHON

#ifdef _WIN64

#include <iostream>

#include "EAD_infos.h"
#include <Python.h>



// It's the encapsulation of the policy class for python

//d�finition de la structure ObjetPython
typedef struct {
    PyObject_HEAD 
	EAD_infos* m_EAD_info;
    /* Type-specific fields go here. */
} PyEAD_infos;






static PyObject *
get_original_maturity(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->original_maturity);	
}

static PyObject *
get_ead_drawn(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->ead_drawn);	
}

static PyObject *
get_residual_maturity(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->residual_maturity);	
}

static PyObject *
get_fair_value(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->fair_value);	
}

static PyObject *
get_accrued_interests(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->accrued_interests);	
}

static PyObject *
get_past_due_int(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->past_due_int);	
}

static PyObject *
get_past_due_ppal(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->past_due_ppal);	
}

static PyObject *
get_accrued_fees(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->accrued_fees);	
}

static PyObject *
get_draw_proba(PyEAD_infos *self)
{
	return PyFloat_FromDouble( self->m_EAD_info->draw_proba);	
}

static PyObject* set_ead_calc(PyEAD_infos* self, PyObject* args)
{
    double ead_calc;
    PyArg_ParseTuple(args, "d", &ead_calc);
	self->m_EAD_info->ead_calc = ead_calc;
	return (PyObject*) self;
}





// on d�finit la table des m�thodes pour l'ObjetPython
// on rend accessible l'accesseur du montant de la police
static PyMethodDef objpy_methods[] = {
	{"set_ead_calc", (PyCFunction)set_ead_calc, METH_VARARGS,""},
	{"original_maturity", (PyCFunction)get_original_maturity, METH_NOARGS,""},
	{"ead_drawn", (PyCFunction)get_ead_drawn, METH_NOARGS,""},
	{"residual_maturity", (PyCFunction)get_residual_maturity, METH_NOARGS,""},
	{"fair_value", (PyCFunction)get_fair_value, METH_NOARGS,""},
	{"accrued_interests", (PyCFunction)get_accrued_interests, METH_NOARGS,""},
	{"past_due_int", (PyCFunction)get_past_due_int, METH_NOARGS,""},
	{"past_due_ppal", (PyCFunction)get_past_due_ppal, METH_NOARGS,""},
	{"accrued_fees", (PyCFunction)get_accrued_fees, METH_NOARGS,""},
	{"draw_proba", (PyCFunction)get_draw_proba, METH_NOARGS,""},
    {NULL}  /* Sentinel */
};



// on d�finit l'object type d'un ObjetPython
static PyTypeObject PyEAD_infosType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "PyEAD_infos",             /*tp_name*/
    sizeof(PyEAD_infos), /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    0, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,			/*tp_flags*/
    "PyEAD_infos",           /* tp_doc */
    0,							/* tp_traverse */
    0,						/* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    objpy_methods,             /* tp_methods */
    0,						/* tp_members */
    0,						/* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,						/* tp_init */
    0,                         /* tp_alloc */
    0,                 /* tp_new */
};

#endif
#endif // NO_PYTHON